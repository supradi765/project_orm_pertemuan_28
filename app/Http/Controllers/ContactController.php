<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function contact(){
        $listContact = Contact::select('id', 'name', 'email', 'number_phone')->get();
        return view('contact.halaman_contact', compact('listContact'));
    }
    public function form(){
        return view('contact.halaman_form');
    }

    public function simpanData(Request $request){
        try {
            $datas = $request->all();
            $contact = new Contact;
            $contact->name = $datas['name'];
            $contact->email = $datas['email'];
            $contact->number_phone = $datas['number_phone'];
            $contact->save();
            return redirect()->route('contact.contact')->with('success', __('Berhasil'));
        } catch (\Throwable $th) {
            return redirect()->route('contact.form-input')->with('error', __($th->getMessage()));
        }
    }

    public function formEdit($id){
        $getDataById = Contact::select('id','name','email','number_phone')->where('id', $id)->first();

        return view('contact.halaman_edit', compact('getDataById'));
    }

    public function editData(Request $req, $id){
        try {
            $datas = $req->all();
            Contact::where('id', $id)->update(
                [
                    'name' => $datas['name'],
                    'email' => $datas['email'],
                    'number_phone' =>$datas['number_phone']
                ]
                );
                return redirect()->route('contact.contact')->with('success', __('Berhasil edit data'));
        }catch (\Throwable $th) {
            return redirect()->route('contact.form-edit', $id)->with('error', __($th->getMessage()));
        }
    }

    public function deleteData($id){
        try{
            Contact::where('id', $id)->delete();

            return redirect()->route('contact.contact')->with('success', __('Berhasil hapus data'));
        }catch (\Throwable $th) {
            return redirect()->route('contact.contact', $id)->with('error', __($th->getMessage()));
        }
    }
}

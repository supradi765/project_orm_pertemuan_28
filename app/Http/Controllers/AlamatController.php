<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Alamat;


class AlamatController extends Controller
{
    public function alamat(){
        $dataAlamat = Alamat::select('alamat.id', 'alamat.alamat','contact.email')
        ->join('contact', 'contact.id', '=', 'alamat.id_contact')->get();
        return view('alamat.alamat', compact('dataAlamat'));

    }
    public function formAlamat(){
        $dataContact = Contact::select('id', 'email')->get();
        return view('alamat.form', compact('dataContact'));
    }

    public function simpanData(Request $req){
        try{
            $data = $req->all();
            $save = new Alamat;
            $save->alamat = $data['alamat'];
            $save->id_contact = $data['id_contact'];
            $save->save();
            return redirect()->route('alamat.alamat')->with('sucses', __('berhasil membuat data'));
        }catch (\Throwable $th) {
            return redirect()->route('alamat.form')->with('error', __($th->getMessage()));
        }
    }

    public function deleteData($id){
        try{
            Alamat::where('id', $id)->delete();

            return redirect()->route('alamat.alamat')->with('success', __('Berhasil hapus data'));
        }catch (\Throwable $th) {
            return redirect()->route('alamat.alamat', $id)->with('error', __($th->getMessage()));
        }
    }



}

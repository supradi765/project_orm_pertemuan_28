<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AlamatController;
use App\Http\Controllers\SessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(ContactController::class)->name('contact.')->prefix('master/contact')->group(function()
    {Route::get('/','contact')->name('contact');
     Route::get('/form','form')->name('form-input');
     Route::post('/simpandata','simpanData')->name('simpan-data');
     Route::get('/form-edit/{id}','formEdit')->name('form-edit');
     Route::post('/edit/{id}','editData')->name('simpan-edit');
     Route::get('/delete-data/{id}','deleteData')->name('delete-data');
 });

 Route::controller(AlamatController::class)->name('alamat.')->prefix('master/alamat')->group(function()
    {Route::get('/','alamat')->name('alamat');
     Route::get('/form','formAlamat')->name('form');
     Route::post('/simpandata','simpanData')->name('simpan-data');
     Route::get('/delete-data/{id}','deleteData')->name('delete-data');
 });


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

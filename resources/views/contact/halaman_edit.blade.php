<!DOCTYPE html>
<html>
<head>
  <title>Form edit</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('error') }}
    </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('success') }}
        </div>
    @endif
  <div class="container">
    <h2 class="mt-4">Silahkan edit data</h2>
    <form action="{{ route('contact.simpan-edit', $getDataById->id) }}" method="post">
        @csrf
        <div class="card">
            <div class="card-header">
                <div class="form-group">
                    <label for="name">Nama:</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $getDataById->name }}" />
                  </div>

                  <div class="form-group">
                    <label for="email">Alamat Email:</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $getDataById->email }}" />
                  </div>

                  <div class="form-group">
                    <label for="phone">Nomor Telepon:</label>
                    <input type="text" class="form-control" id="number_phone" name="number_phone" value="{{ $getDataById->number_phone }}"/>
                  </div>

                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button class="btn btn-primary"><a href="{{ route('contact.contact') }}" class="text-white">kembali</a></button>
                </form>
              </div>

            </div>
        </div>


  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>

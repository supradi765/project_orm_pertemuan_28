<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Form Input</title>
</head>
<body>
    <p>
        @if (session('error'))
        {{ session('error') }}
      @endif
      @if(session('sucses'))
      {{ session('sucses') }}
    @endif
    </p>

      <div class="container">
        <h2 class="mt-4">Silahkan menambahkan data</h2>

      <div class="card">
        <div class="card-body">
            <form action="{{ route('alamat.simpan-data') }}" method="POST">
            @csrf
            <textarea name="alamat" id="alamat" placeholder="isi alamat lengkap" cols="30" rows="10" value="{{ old('alamat') }}"></textarea>

            <br>
                <select name="id_contact" id="id_contact">
                    <option value="">Pilih email</option>
                    @foreach ($dataContact as $row)
                        <option value="{{ $row->id }}">{{ $row->email }}</option>
                    @endforeach
                </select>
                <br>
                <button type="submit" value="Simpan">simpan</button>
                <button typer="submit"><a href="{{ route('alamat.alamat') }}" class="text-dark">Kembali</a></button>
            </form>
          </div>
        </div>
      </div>

      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>

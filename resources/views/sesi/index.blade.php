<!DOCTYPE html>
<html>
<head>
  <title>Form Login</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      background-color: #f8f9fa;
    }

    .login-container {
      max-width: 400px;
      margin: 0 auto;
      margin-top: 100px;
      padding: 20px;
      border: 1px solid #ddd;
      background-color: #fff;
      border-radius: 5px;
    }

    .login-container h2 {
      text-align: center;
      margin-bottom: 20px;
    }

    .login-container .form-group {
      margin-bottom: 20px;
    }

    .login-container label {
      font-weight: bold;
    }

    .login-container .btn-login {
      width: 100%;
    }
  </style>
</head>
<body>
    <p>@if (session('error'))
        {{ session('error') }}
      @endif
      @if(session('sucses'))
        {{ session('sucses') }}
      @endif</p>
  <div class="container">
    <div class="login-container">
      <h2>Form Login</h2>
      <form action="/sesi/alamat" method="POST">
        @csrf
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" name="email" class="form-control" id="email" required>
        </div>
        <div class="form-group">
          <label for="password">Password:</label>
          <input type="password" name="paswoord" class="form-control" id="password" required>
        </div>
        <button type="submit" name="submit" class="btn btn-primary btn-login">Login</button>
      </form>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
